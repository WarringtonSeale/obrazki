clc;
clear;
close all;

%% 
% Wczytywanie obrazu

img = imread('morda.bmp');
img2 = imread('morda.bmp');
imshow(img);title('Obraz początkowy 1')
%imshow(img2);title('Obraz początkowy 2')

%% Przekształcenia geometryczne
% skalowanie:

imgscaled = imresize(img,0.69);
imshow(imgscaled);title(['Obraz przeskalowany'])
%% 
% obracanie:

imgrotated = imrotate(img, 420);
imshow(imgrotated);title('Obraz obrócony')
%% 
% kadrowanie:

imgcropped = imcrop(img,[440, 168, 300, 200]);
imshow(imgcropped); title('Obraz wykadrowany')
%% 
% przekształcenie zaawansowane:

imgtransformed = imtransform(img, maketform('affine',[1 0 0; .5 1 0; 0 0 1]));
imshow(imgtransformed); title('Obraz po transformacji ścięcia')
%% Przekształcenia punktowe:
% dodawanie stałej:

imgadded = imadd(img, 30);
imshow(imgadded);title('Obraz z dodaną stałą')
%% 
% odejmowanie stałej:

imgremoved = imadd(img, -30);
imshow(imgremoved);title('Obraz z odjętą stałą')
%% 
% mnożenie przez stałą:

imgmultiplied = immultiply(img, 1.3);
imshow(imgmultiplied);title('Obraz wymnożony przez stałą')
%% 
% dwuargumentowa operacja punktowa:

imgdouble = imabsdiff(img, imgremoved);
imgdouble = imadjust(imgdouble, [0, 0.1], [0, 1]);
imshow(imgdouble); title('Dwuargumentowa operacja')
%% 
% suma dwóch obrazów:

imgsum = imadd(img, img2);
imshow(imgsum);title('Suma dwóch obrazów')
%% 
% wyrównanie histogramu:

imghiseq = histeq(img);
imshow(imghiseq),title('Obraz z wyrównanym histogramem')
imhist(img);title('Histogram obrazu początkowego')
imhist(imghiseq);title('Histogram obrazu wyrównanego')
%% 
% modulacja gamma:

imggammaadjust = imadjust(img, [0.3, 0.7], []);
imshow(imggammaadjust);title('Obraz z modulacją gamma')
%% 
% binaryzacja:

imgbw = im2bw(img);
imshow(imgbw);title('Obraz po binaryzacji')
%% 
% transformata fouriera:

fft = abs(fftshift(fft2(rgb2gray(img))));
fftnorm = 255*fft/max(max(fft));
imshow(fftnorm);title('Znormalizowane widmo transformaty fouriera')
%% 
% filtr dolnoprzepustowy:

cm = ones(10)/100;
imglowpass = imfilter(img, cm);
imshow(imglowpass);title('Obraz po filtracji dolnoprzepustowej')
%% 
% filtr górnoprzepustowy:

cm = [0, 0, 0; -1, 0, 0; 0, 1, 0];
imghighpass = imfilter(img, cm);
imshow(imghighpass);title('Obraz po filtracji górnoprzepustowej')
%% 
% filtr robertsa:

cm = [-1, -1, -1; 0, 0, 0; 1, 1, 1];
imgroberts = imfilter(img, cm);
imshow(imgroberts);title('Obraz po filtracji filtrem robertsa')
%% 
% maska Sobela:

cm = [-1, -2, -1; 0, 0, 0; 1, 2, 1];
imgsobel=imfilter(img, cm);
imshow(imgsobel);title('Obraz po nałożeniu maski sobela')
%% 
% maska Brewitta

cm=[-1 1 -1;0 0 0;1 1 1];
imgbrewitt = imfilter(img, cm);
imshow(imgbrewitt);title('Obraz po nałożeniu maski brewitta')
%% 
% Laplacean

cm=[0 -1 0;-1 4 -1;0 -1 0];
imglaplace = imfilter(img, cm);
imshow(imglaplace);title('Obraz po filtracji Laplacea')
%% 
% filtr rozmazujacy:

f = fspecial('unsharp');
imgunsharpened=imfilter(img, f);title('Filtr rozmazujacy')
%% 
% filtr usredniajacy:

f= fspecial('average');
imgaverage = imfilter(img, f);
imshow(imgaverage);title('Obraz po zastosowaniu filtra uśredniającego')
%% 
% filtr medianowy

imgwithnoise = imnoise(img,'salt & pepper',0.05);
noisefiltered = medfilt3(imgwithnoise);
imshow(imgwithnoise);title('zakłócenia')
imshow(noisefiltered);title('zakłócenia po filtracji medianowej')
%% 
% erozja pionowo:

imgeroded = imerode(img,strel('line', 11, 90));
imshow(imgeroded);title('obraz poddany erozji w pionie')
%% 
% erozja poziomo:

imgerodedh = imerode(img,strel('line', 11, 0));
imshow(imgerodedh);title('obraz poddany erozji w poziomie')
%% 
% dylatacja:

imgdilatedline = imdilate(img, strel('line', 11, 90));
imshow(imgdilatedline);title('dylatacja liniowa')
imgdilateddisk = imdilate(img, strel('disk', 11));
imshow(imgdilateddisk);title('dylatacja dysk')
imgdilatedarb = imdilate(img, strel('arbitrary',ones(3)));
imshow(imgdilatedarb);title('dylatacja arbitralna ')
%% 
% pocienianie:

imgmorph = bwmorph(im2bw(img,[100/255]),'remove');
imshow(imgmorph);title('obraz po "pocienianiu"')
%% 
% Szkieletyzacja:

imgskel = bwmorph(im2bw(img, [100/255]), 'skel');
imshow(imgskel);title('obraz po szkieletyzacji');
%% 
% Centroidy:

imgcentr = bwmorph(im2bw(img, [100/255]), 'remove');
imgcentr = bwmorph(imgcentr, 'shrink');
imshow(imgcentr);title('centroidy')
%% 
% Wykrywanie naroznikow

fil = [1, 1, 1; -1, -2, 1; -1, -1, 1];
imgcorur = imadjust(imfilter(img, fil), stretchlim(img));
imshow(imgcorur);title('wykrywanie prawych gornych naroznikow')

fil = [1, -1, -1; 1, -2, -1; 1, 1, 1];
imgcorll = imadjust(imfilter(img,  fil), stretchlim(img));
imshow(imgcorll);title('wykrywanie naroznikow lewych dolnych')
%% 
% Pomiar odległości

%figure(1)
%imshow(img)
%hold on;
%plot(190, 233, 'r+', 190, 253, 'r+', 'MarkerSize', 30)
%% 
% s


sens=0.9;

[center, radius] = imfindcircles(img, [10, 30], "ObjectPolarity","dark", 'Sensitivity',sens);
imshow(img)
circles=viscircles(center, radius);
dx = abs(center(1, 1)-center(2, 1));
dy = abs(center(1, 2)-center(2, 2));
sprintf('Oko-oko:\ndx: %f, dy: %f, dist: %f', dx, dy, sqrt(dx^2+dy^2));
d = imdistline(gca, center(:,1)',center(:,2)');


det = vision.CascadeObjectDetector('Nose');
nosebox = det(img);
nos = [nosebox(1,1)+0.5*nosebox(1,3), nosebox(1,2)+0.5*nosebox(1,4)];
hold on
plot(nos(1), nos(2), 'r+','MarkerSize', 30)
d2 = imdistline(gca, [center(1,1), nos(1)],[center(1,2), nos(2)]);
d3 = imdistline(gca, [center(2,1), nos(1)],[center(2,2), nos(2)]);
d4 = imdistline(gca, [190, 190], [233, 253])
%plot(mouthbox(1,1), mouthbox(1,2), 'r+', mouthbox(1,1)+mouthbox(1,3), mouthbox(1,2), 'r+');
plot(190, 233, 'r+', 190, 253, 'r+', 'MarkerSize', 30)