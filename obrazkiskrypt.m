clc;
clear;
close all;
figure(1)
%Wczytywanie obrazu
img = imread('morda.bmp');
img2 = imread('morda.bmp');
%imshow(img);%title('Obraz poczatkowy');
%imshow(img2);%title('Obraz poczatkowy 2')
%%title('Obraz poczatkowy');
figure(2);

%Przeksztalcenia geometryczne
%skalowanie:
imgscaled = imresize(img,0.69);
imshow(imgscaled); 
%title('Obraz przeskalowany');


%obracanie:
figure(3)
imgrotated = imrotate(img, 420);
imshow(imgrotated);
%title('Obraz obrocony');
%kadrowanie:
figure(4)
imgcropped = imcrop(img,[440, 168, 300, 200]);
imshow(imgcropped);
%title('Obraz wykadrowany');
figure(5)
%przeksztalcenie zaawansowane:
imgtransformed = imtransform(img, maketform('affine',[1 0 0; .5 1 0; 0 0 1]));
imshow(imgtransformed); %title('Obraz po transformacji sciecia');
%Przeksztalcenia punktowe:
figure(6)
%dodawanie stalej:
imgadded = imadd(img, 30);
imshow(imgadded);%title('Obraz z dodana stala');
%odejmowanie stalej:
figure(7)
imgremoved = imadd(img, -30);
imshow(imgremoved);%title('Obraz z odjeta stala');
%mnozenie przez stala:
figure(8)
imgmultiplied = immultiply(img, 1.3);
imshow(imgmultiplied);%title('Obraz wymnozony przez stala');
%dwuargumentowa operacja punktowa:;
figure(9)
imgdouble = imabsdiff(img, imgremoved);
imgdouble = imadjust(imgdouble, [0, 0.1], [0, 1]);
imshow(imgdouble); %title('Dwuargumentowa operacja');
%suma dwoch obrazow:
figure(10)
imgsum = imadd(img, img2);;
imshow(imgsum);%title('Suma dwoch obrazow');
%wyrownanie histogramu:
figure(11)
imghiseq = histeq(img);;
imshow(imghiseq),%title('Obraz z wyrownanym histogramem');
figure(12)
imhist(img);%title('Histogram obrazu poczatkowego');
figure(13)
imhist(imghiseq);%title('Histogram obrazu wyrownanego');
%modulacja gamma:
figure(14)
imggammaadjust = imadjust(img, [0.3, 0.7], []);
imshow(imggammaadjust);%title('Obraz z modulacja gamma');
%binaryzacja:
figure(15)
imgbw = im2bw(img);
imshow(imgbw);%title('Obraz po binaryzacji')
%transformata fouriera:
figure(16)
fft = abs(fftshift(fft2(rgb2gray(img))));
fftnorm = 255*fft/max(max(fft));
imshow(fftnorm);%title('Znormalizowane widmo transformaty fouriera')
%filtr dolnoprzepustowy:
figure(17)
cm = ones(10)/100;
imglowpass = imfilter(img, cm);
imshow(imglowpass);%title('Obraz po filtracji dolnoprzepustowej')
%filtr gornoprzepustowy:
figure(18)
cm = [0, 0, 0; -1, 0, 0; 0, 1, 0];
imghighpass = imfilter(img, cm);
imshow(imghighpass);%title('Obraz po filtracji gornoprzepustowej')
%filtr robertsa:
figure(19)
cm = [-1, -1, -1; 0, 0, 0; 1, 1, 1];
imgroberts = imfilter(img, cm);
imshow(imgroberts);%title('Obraz po filtracji filtrem robertsa')
%maska Sobela:
figure(20)
cm = [-1, -2, -1; 0, 0, 0; 1, 2, 1];
imgsobel=imfilter(img, cm);
imshow(imgsobel);%title('Obraz po nalozeniu maski sobela')
%maska Brewitta
figure(21)
cm=[-1 1 -1;0 0 0;1 1 1];
imgbrewitt = imfilter(img, cm);
imshow(imgbrewitt);%title('Obraz po nalozeniu maski brewitta')
%Laplacean
figure(22)
cm=[0 -1 0;-1 4 -1;0 -1 0];
imglaplace = imfilter(img, cm);
imshow(imglaplace);%title('Obraz po filtracji Laplacea')
%filtr rozmazujacy:
figure(23)
f = fspecial('unsharp');
imgunsharpened=imfilter(img, f);%title('Filtr rozmazujacy')
%filtr usredniajacy:
figure(24)
f= fspecial('average');
imgaverage = imfilter(img, f);
imshow(imgaverage);%title('Obraz po zastosowaniu filtra usredniajacego')
%filtr medianowy
figure(25)
imgwithnoise = imnoise(img,'salt & pepper',0.05);
noisefiltered = medfilt3(imgwithnoise);
imshow(imgwithnoise);%title('zaklocenia')
figure(26)
imshow(noisefiltered);%title('zaklocenia po filtracji medianowej')
figure(27)
%erozja pionowo:
imgeroded = imerode(img,strel('line', 11, 90));
imshow(imgeroded);%title('obraz poddany erozji w pionie')
figure(28)
%erozja poziomo:
imgerodedh = imerode(img,strel('line', 11, 0));
imshow(imgerodedh);%title('obraz poddany erozji w poziomie')
figure(29)
%dylatacja:
imgdilatedline = imdilate(img, strel('line', 11, 90));
imshow(imgdilatedline);%title('dylatacja liniowa')
figure(30)
imgdilateddisk = imdilate(img, strel('disk', 11));
figure(31)
imshow(imgdilateddisk);%title('dylatacja dysk')
imgdilatedarb = imdilate(img, strel('arbitrary',ones(3)));
figure(32)
imshow(imgdilatedarb);%title('dylatacja arbitralna ')
figure(33)
%pocienianie:
imgmorph = bwmorph(im2bw(img,[100/255]),'remove');
imshow(imgmorph);%title('obraz po "pocienianiu"')
figure(34)
%Szkieletyzacja:
imgskel = bwmorph(im2bw(img, [100/255]), 'skel');
imshow(imgskel);%title('obraz po szkieletyzacji');
figure(35)
%Centroidy:
imgcentr = bwmorph(im2bw(img, [100/255]), 'remove');
imgcentr = bwmorph(imgcentr, 'shrink');
imshow(imgcentr);%title('centroidy')
figure(36)
%Wykrywanie naroznikow
fil = [1, 1, 1; -1, -2, 1; -1, -1, 1];
imgcorur = imadjust(imfilter(img, fil), stretchlim(img));
imshow(imgcorur);%title('wykrywanie prawych gornych naroznikow')
figure(37)
fil = [1, -1, -1; 1, -2, -1; 1, 1, 1];
imgcorll = imadjust(imfilter(img,  fil), stretchlim(img));
imshow(imgcorll);%title('wykrywanie naroznikow lewych dolnych')
%figure(38)
%Pomiar o6dleglosci
%figure(1)
%imshow(img)
%hold on;
%plot(190, 233, 'r+', 190, 253, 'r+', 'MarkerSize', 30)
%s
% figure(37)
% sens=0.9;
% 
% [center, radius] = imfindcircles(img, [10, 30], "ObjectPolarity","dark", 'Sensitivity',sens);
% imshow(img)
% circles=viscircles(center, radius);
% dx = abs(center(1, 1)-center(2, 1));
% dy = abs(center(1, 2)-center(2, 2));
% sprintf('Oko-oko:\ndx: %f, dy: %f, dist: %f', dx, dy, sqrt(dx^2+dy^2));
% d = imdistline(gca, center(:,1)',center(:,2)');
% 
% 
% det = vision.CascadeObjectDetector('Nose');
% nosebox = det(img);
% nos = [nosebox(1,1)+0.5*nosebox(1,3), nosebox(1,2)+0.5*nosebox(1,4)];
% hold on
% plot(nos(1), nos(2), 'r+','MarkerSize', 30)
% d2 = imdistline(gca, [center(1,1), nos(1)],[center(1,2), nos(2)]);
% d3 = imdistline(gca, [center(2,1), nos(1)],[center(2,2), nos(2)]);
% det = vision.CascadeObjectDetector('Mouth');
% mouthbox = det(img,[238, 263, 245, 137]);
% plot(mouthbox(1,1), mouthbox(1,2), 'r+', mouthbox(1,1)+mouthbox(1,3), mouthbox(1,2), 'r+');

